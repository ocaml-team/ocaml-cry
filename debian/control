Source: ocaml-cry
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Kyle Robbertze <paddatrapper@debian.org>
Build-Depends: debhelper-compat (=13),
               ocaml-dune,
               dh-ocaml (>= 1.2),
               ocaml,
               libfindlib-ocaml,
               libssl-ocaml-dev (>= 0.5.0)
Standards-Version: 4.7.0
Homepage: https://www.liquidsoap.info/
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-cry.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-cry
Rules-Requires-Root: no

Package: libcry-ocaml-dev
Architecture: any
Depends: ${ocaml:Depends}, ocaml-findlib, ${misc:Depends}, ${shlibs:Depends}
Provides: ${ocaml:Provides}
Description: MP3/Ogg Vorbis broadcast OCaml module
 This OCaml module implements the protocols used to connect and
 send source data to icecast2 and shoutcast servers.
 .
 It is a low-level implementation, so it only does the minimal
 source connection. In particular, it does not handle synchronisation.
 Hence, the task of sending audio data to the streaming server
 at real time rate is up to the programmer, contrary to the main
 implementation, libshout.
